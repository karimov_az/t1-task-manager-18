package ru.t1.karimov.tm.util;

import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() throws AbstractException {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new NumberIncorrectException(value, e);
        }
    }

}
