package ru.t1.karimov.tm.api.service;

import ru.t1.karimov.tm.exception.AbstractException;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId) throws AbstractException;

    void removeProjectById(String projectId) throws AbstractException;

    void unbindTaskFromProject(String projectId, String taskId) throws AbstractException;

}
