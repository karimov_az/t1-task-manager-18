package ru.t1.karimov.tm.command.user;

import ru.t1.karimov.tm.exception.AbstractException;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public void execute() throws AbstractException {
        serviceLocator.getAuthService().logout();
        System.out.println("USER LOGOUT");
    }

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public String getDescription() {
        return "User logout.";
    }

}
