package ru.t1.karimov.tm.command.system;

import ru.t1.karimov.tm.api.service.ICommandService;
import ru.t1.karimov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
