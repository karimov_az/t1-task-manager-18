package ru.t1.karimov.tm.command.user;

import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand{

    @Override
    public void execute() throws AbstractException {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

    @Override
    public String getName() {
        return "user-change-password";
    }

    @Override
    public String getDescription() {
        return "Change password of current user.";
    }

}
