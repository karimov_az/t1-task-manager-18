package ru.t1.karimov.tm.command.user;

import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public void execute() throws AbstractException {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @Override
    public String getName() {
        return "user-view-profile";
    }

    @Override
    public String getDescription() {
        return "View profile of current user.";
    }

}
